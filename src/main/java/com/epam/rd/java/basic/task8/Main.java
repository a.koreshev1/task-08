package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.DOMController;
import com.epam.rd.java.basic.task8.controller.SAXController;
import com.epam.rd.java.basic.task8.controller.STAXController;
import com.epam.rd.java.basic.task8.flower.Flower;

import java.util.Comparator;

public class Main {

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            return;
        }

        String xmlFileName = args[0];
        System.out.println("Input ==> " + xmlFileName);

        ////////////////////////////////////////////////////////
        // DOM
        ////////////////////////////////////////////////////////

        // get container
        DOMController domController = new DOMController(xmlFileName);
        domController.parseXmlFile();
        domController.getContainer();

        // sort (case 1)
        domController.sort(Comparator.comparing(Flower::getName)
                .thenComparing(flower -> flower.getGrowingTips().getLightingAttr().getAttrValue(),
						Comparator.reverseOrder()));

        // save
        String outputXmlFile = "output.dom.xml";
        domController.createXmlFile(outputXmlFile);

        ////////////////////////////////////////////////////////
        // SAX
        ////////////////////////////////////////////////////////

        // get
        SAXController saxController = new SAXController(xmlFileName);
        saxController.parseXmlFile();

        // sort  (case 2)
        saxController.sort(Comparator.comparing(Flower::getName));

        // save
        outputXmlFile = "output.sax.xml";
        saxController.createXmlFile(outputXmlFile);

        ////////////////////////////////////////////////////////
        // StAX
        ////////////////////////////////////////////////////////

        // get
        STAXController staxController = new STAXController(xmlFileName);
        staxController.parseXmlFile();

        // sort  (case 3)
        staxController.sort(Comparator.comparing(flower -> flower.getGrowingTips().getTempreture()));

        // save
        outputXmlFile = "output.stax.xml";
        staxController.createXmlFile(outputXmlFile);
    }
}
