package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.flower.Flower;
import com.epam.rd.java.basic.task8.flower.GrowingTips;
import com.epam.rd.java.basic.task8.flower.TagAttribute;
import com.epam.rd.java.basic.task8.flower.VisualParameters;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController implements XMLController<Flower> {

	private final String xmlFileName;
	private final List<Flower> flowers = new ArrayList<>();
	private Document document;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	@Override
	public void xmlValidate(String xmlFilename) throws SAXException {
		XMLValidator xmlValidator = new XMLValidator(xmlFilename, XSD_FILE);
		if (!xmlValidator.validate()) {
			throw new SAXException();
		}
	}


	/**
	 * Parse the content of the xml file as an XML document
	 * and return a new DOM {@link Document} object.
	 *
	 * @throws ParserConfigurationException Indicates configuration error
	 * @throws SAXException if the xsd file is wrong or the xml file is not valid
	 * @throws IOException  if cannot read the xml file
	 */
	@Override
	public void parseXmlFile() {
		try {
			xmlValidate(xmlFileName);
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			document = builder.parse(new File(xmlFileName));
		} catch (ParserConfigurationException | IOException | SAXException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Creates Flower object from xml file
	 *
	 * @param nodes it is tree that contains entire xml's entities
	 * @return Flower with filled all fields
	 */
	public Flower createFlower(NodeList nodes) {
		String name = nodes.item(1).getTextContent();
		String soil = nodes.item(3).getTextContent();
		String origin = nodes.item(5).getTextContent();
		VisualParameters visualParameters = createVisualParameters(nodes);
		GrowingTips growingTips = createGrowingTips(nodes);
		String multiplying = nodes.item(11).getTextContent();
		return new Flower(name, soil, origin, visualParameters, growingTips, multiplying);
	}

	/**
	 * Creates VisualParameters object that is field of Flower object
	 *
	 * @param nodes it is tree that contains entire xml's entities
	 * @return VisualParameters object with filled all fields
	 */
	public VisualParameters createVisualParameters(NodeList nodes) {
		String stemColour = nodes.item(7).getChildNodes().item(1).getTextContent();
		String leafColour = nodes.item(7).getChildNodes().item(3).getTextContent();
		int aveLenFlower = Integer.parseInt(nodes.item(7).getChildNodes().item(5).getTextContent());
		String attrName = nodes.item(7).getChildNodes().item(5).getAttributes().getNamedItem("measure").getNodeName();
		String attrValue = nodes.item(7).getChildNodes().item(5).getAttributes().getNamedItem("measure").getNodeValue();
		TagAttribute aveLenFlowerAttr = new TagAttribute(attrName, attrValue);
		return new VisualParameters(stemColour, leafColour, aveLenFlower, aveLenFlowerAttr);
	}

	/**
	 * Creates GrowingTips object that is field of Flower object
	 *
	 * @param nodes it is tree that contains entire xml's entities
	 * @return GrowingTips object with filled all fields
	 */
	public GrowingTips createGrowingTips(NodeList nodes) {
		int tempreture = Integer.parseInt(nodes.item(9).getChildNodes().item(1).getTextContent());
		String tempretureAttrName = nodes.item(9).getChildNodes().item(1).getAttributes().getNamedItem("measure").getNodeName();
		String tempretureAttrValue = nodes.item(9).getChildNodes().item(1).getAttributes().getNamedItem("measure").getNodeValue();
		String lighting = nodes.item(9).getChildNodes().item(3).getTextContent();
		String lightingAttrName = nodes.item(9).getChildNodes().item(3).getAttributes().getNamedItem("lightRequiring").getNodeName();
		String lightingAttrValue = nodes.item(9).getChildNodes().item(3).getAttributes().getNamedItem("lightRequiring").getNodeValue();
		int watering = Integer.parseInt(nodes.item(9).getChildNodes().item(5).getTextContent());
		String wateringAttrName = nodes.item(9).getChildNodes().item(5).getAttributes().getNamedItem("measure").getNodeName();
		String wateringAttrValue = nodes.item(9).getChildNodes().item(5).getAttributes().getNamedItem("measure").getNodeValue();
		TagAttribute tempretureAttr = new TagAttribute(tempretureAttrName, tempretureAttrValue);
		TagAttribute wateringAttr = new TagAttribute(lightingAttrName, lightingAttrValue);
		TagAttribute lightingAttr = new TagAttribute(wateringAttrName, wateringAttrValue);
		return new GrowingTips(tempreture, lighting, watering, tempretureAttr, lightingAttr, wateringAttr);
	}

	@Override
	public List<Flower> getContainer() {
		NodeList container = document.getDocumentElement().getElementsByTagName("flower");
		for (int i = 0; i < container.getLength(); i++) {
			Node item = container.item(i);
			flowers.add(createFlower(item.getChildNodes()));
		}
		return flowers;
	}

	@Override
	public void sort(Comparator<? super Flower> flower) {
		flowers.sort(flower);
	}

	@Override
	public void createXmlFile(String outputXmlFile) {
		XMLFileCreator xmlFileCreator = new XMLDocumentFileCreator(flowers);
		xmlFileCreator.createXmlFile(outputXmlFile);
		try {
			xmlValidate(outputXmlFile);
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}
}
