package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.flower.Flower;
import com.epam.rd.java.basic.task8.flower.GrowingTips;
import com.epam.rd.java.basic.task8.flower.TagAttribute;
import com.epam.rd.java.basic.task8.flower.VisualParameters;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler implements XMLController<Flower> {
    private final String xmlFileName;
    private final StringBuilder currentElementText = new StringBuilder();
    private List<Flower> flowers;
    private Flower flower = new Flower();
    private TagAttribute attribute = new TagAttribute();
    private VisualParameters visualParameters = new VisualParameters();
    private GrowingTips growingTips = new GrowingTips();

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    @Override
    public void xmlValidate(String xmlFilename) throws SAXException {
        XMLValidator xmlValidator = new XMLValidator(xmlFilename, XSD_FILE);
        if (!xmlValidator.validate()) {
            throw new SAXException();
        }
    }

    /**
     * Parse the content of the xml file with handler as SAXController object.
     * Creates container from xml file that represents list of Flower objects
     *
     * @throws ParserConfigurationException Indicates configuration error
     * @throws SAXException if the xsd file is wrong or the xml file is not valid
     * @throws IOException  if cannot read the xml file
     */
    @Override
    public void parseXmlFile() {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            xmlValidate(xmlFileName);
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse(xmlFileName, this);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void startDocument() {
        flowers = new ArrayList<>();
    }

    /**
     * It creates new empty Flower object to clear early created Flower object
     * It parses and creates attribute from xml tag
     *
     * @param uri The Namespace URI, or the empty string if the
     *        element has no Namespace URI or if Namespace
     *        processing is not being performed.
     * @param localName The local name (without prefix), or the
     *        empty string if Namespace processing is not being
     *        performed.
     * @param qName The qualified name (with prefix), or the
     *        empty string if qualified names are not available.
     */
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        currentElementText.setLength(0);
        if (qName.equalsIgnoreCase("flower")) {
            flower = new Flower();
            visualParameters = new VisualParameters();
            growingTips = new GrowingTips();
        }
        String attrName = attributes.getQName(0);
        attribute = new TagAttribute(attrName, attributes.getValue(attrName));
    }

    /**
     * Creates Flower object from xml file
     * Creates container from xml file that represents list of Flower objects
     *
     * @param uri The Namespace URI, or the empty string if the
     *        element has no Namespace URI or if Namespace
     *        processing is not being performed.
     * @param localName The local name (without prefix), or the
     *        empty string if Namespace processing is not being
     *        performed.
     * @param qName The qualified name (with prefix), or the
     *        empty string if qualified names are not available.
     */
    @Override
    public void endElement(String uri, String localName, String qName) {
        String currentFlowerProperty = currentElementText.toString();
        switch (qName) {
            case "name":
                flower.setName(currentFlowerProperty);
                break;
            case "soil":
                flower.setSoil(currentFlowerProperty);
                break;
            case "origin":
                flower.setOrigin(currentFlowerProperty);
                break;
            case "stemColour":
                visualParameters.setStemColour(currentFlowerProperty);
                break;
            case "leafColour":
                visualParameters.setLeafColour(currentFlowerProperty);
                break;
            case "aveLenFlower":
                visualParameters.setAvelenFlower(Integer.parseInt(currentFlowerProperty));
                visualParameters.setAvelenFlowerAttr(attribute);
                break;
            case "visualParameters":
                flower.setVisualParameters(visualParameters);
                break;
            case "tempreture":
                growingTips.setTempreture(Integer.parseInt(currentFlowerProperty));
                growingTips.setTempretureAttr(attribute);
                break;
            case "lighting":
                growingTips.setLighting(currentFlowerProperty);
                growingTips.setLightingAttr(attribute);
                break;
            case "watering":
                growingTips.setWatering(Integer.parseInt(currentFlowerProperty));
                growingTips.setWateringAttr(attribute);
                break;
            case "growingTips":
                flower.setGrowingTips(growingTips);
                break;
            case "multiplying":
                flower.setMultiplying(currentFlowerProperty);
                break;
            case "flower":
                flowers.add(flower);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        currentElementText.append(ch, start, length);
    }

    @Override
    public void sort(Comparator<? super Flower> flower) {
        flowers.sort(flower);
    }

    @Override
    public List<Flower> getContainer() {
        return flowers;
    }

    @Override
    public void createXmlFile(String outputXmlFile) {
        XMLFileCreator xmlFileCreator = new XMLDocumentFileCreator(flowers);
        xmlFileCreator.createXmlFile(outputXmlFile);
        try {
            xmlValidate(outputXmlFile);
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }
}