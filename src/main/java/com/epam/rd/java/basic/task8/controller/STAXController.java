package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.flower.Flower;
import com.epam.rd.java.basic.task8.flower.GrowingTips;
import com.epam.rd.java.basic.task8.flower.TagAttribute;
import com.epam.rd.java.basic.task8.flower.VisualParameters;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler implements XMLController<Flower> {
    private final String xmlFileName;
    private final List<Flower> flowers;
    private Flower flower;
    private VisualParameters visualParameters;
    private GrowingTips growingTips;

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
        this.flowers = new ArrayList<>();
    }

    @Override
    public void xmlValidate(String xmlFilename) throws SAXException {
        XMLValidator xmlValidator = new XMLValidator(xmlFilename, XSD_FILE);
        if (!xmlValidator.validate()) {
            throw new SAXException();
        }
    }

    /**
     * Parse the content of the xml file.
     * Creates container from xml file that represents list of Flower objects
     *
     * @throws FileNotFoundException if cannot read the xml file
     * @throws SAXException  if the xsd file is wrong or the xml file is not valid
     * @throws XMLStreamException  if there is an error with the underlying XML
     * @throws NullPointerException  if instance of XMLEventReader is null
     */
    @Override
    public void parseXmlFile() throws SAXException, XMLStreamException {
        xmlValidate(xmlFileName);
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLEventReader reader = null;
        try {
            reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (reader == null) throw new NullPointerException();
        while (reader.hasNext()) {
            XMLEvent nextEvent = reader.nextEvent();
            if (nextEvent.isStartElement()) {
                setFlowerProperty(reader, nextEvent);
            }
            if (nextEvent.isEndElement()) {
                EndElement endElement = nextEvent.asEndElement();
                if (endElement.getName().getLocalPart().equals("flower")) {
                    flowers.add(flower);
                }
            }
        }
    }


    /**
     * Fill one field of Flower object
     * It parses and creates attribute from xml tag
     * Ends up creates Flower object from xml file
     *
     * @param reader it provides the ability to peek at the next event
     * @param nextEvent event that can be obtained by XMLEventReader object
     */
    private void setFlowerProperty(XMLEventReader reader, XMLEvent nextEvent) throws XMLStreamException {
        StartElement startElement = nextEvent.asStartElement();
        TagAttribute tagAttribute = new TagAttribute();
        if (startElement.getAttributes().hasNext()) {
            tagAttribute.setAttrName(startElement.getAttributes().next().getName().getLocalPart());
            tagAttribute.setAttrValue(startElement.getAttributes().next().getValue());
        }
        switch (startElement.getName().getLocalPart()) {
            case "flower":
                flower = new Flower();
                visualParameters = new VisualParameters();
                growingTips = new GrowingTips();
                break;
            case "name":
                nextEvent = reader.nextEvent();
                flower.setName(nextEvent.asCharacters().getData());
                break;
            case "soil":
                nextEvent = reader.nextEvent();
                flower.setSoil(nextEvent.asCharacters().getData());
                break;
            case "origin":
                nextEvent = reader.nextEvent();
                flower.setOrigin(nextEvent.asCharacters().getData());
                break;
            case "stemColour":
                nextEvent = reader.nextEvent();
                visualParameters.setStemColour(nextEvent.asCharacters().getData());
                break;
            case "leafColour":
                nextEvent = reader.nextEvent();
                visualParameters.setLeafColour(nextEvent.asCharacters().getData());
                break;
            case "aveLenFlower":
                nextEvent = reader.nextEvent();
                visualParameters.setAvelenFlower(Integer.parseInt(nextEvent.asCharacters().getData()));
                visualParameters.setAvelenFlowerAttr(tagAttribute);
                break;
            case "visualParameters":
                flower.setVisualParameters(visualParameters);
                break;
            case "tempreture":
                nextEvent = reader.nextEvent();
                growingTips.setTempreture(Integer.parseInt(nextEvent.asCharacters().getData()));
                growingTips.setTempretureAttr(tagAttribute);
                break;
            case "lighting":
                growingTips.setLighting("");
                growingTips.setLightingAttr(tagAttribute);
                break;
            case "watering":
                nextEvent = reader.nextEvent();
                growingTips.setWatering(Integer.parseInt(nextEvent.asCharacters().getData()));
                growingTips.setWateringAttr(tagAttribute);
                break;
            case "growingTips":
                flower.setGrowingTips(growingTips);
                break;
            case "multiplying":
                nextEvent = reader.nextEvent();
                flower.setMultiplying(nextEvent.asCharacters().getData());
                break;
        }
    }

    @Override
    public void sort(Comparator<? super Flower> flower) {
        flowers.sort(flower);
    }

    @Override
    public List<Flower> getContainer() {
        return flowers;
    }

    @Override
    public void createXmlFile(String outputXmlFile) {
        XMLFileCreator xmlFileCreator = new XMLDocumentFileCreator(flowers);
        xmlFileCreator.createXmlFile(outputXmlFile);
        try {
            xmlValidate(outputXmlFile);
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }
}