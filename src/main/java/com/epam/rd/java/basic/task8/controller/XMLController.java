package com.epam.rd.java.basic.task8.controller;

import org.xml.sax.SAXException;

import java.util.Comparator;
import java.util.List;

/**
 * Defines methods for xml parsers
 * Determines xsd file for xml parsers
 * Specifies the type of container elements for xml parsers
 *
 * @author Alexander Koreshev
 * @since 1.0
 */
public interface XMLController<T> {
    String XSD_FILE = "input.xsd";

    void xmlValidate(String xmlFilename) throws SAXException;

    void parseXmlFile() throws Exception;

    List<T> getContainer();

    void sort(Comparator<? super T> t);

    void createXmlFile(String outputXmlFile);
}
