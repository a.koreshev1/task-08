package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.flower.Flower;
import com.epam.rd.java.basic.task8.flower.GrowingTips;
import com.epam.rd.java.basic.task8.flower.VisualParameters;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.List;

/**
 * Creates the Document object from obtained container
 * Writes the Document object to the xml file
 *
 * @author Alexander Koreshev
 * @since 1.0
 */
public class XMLDocumentFileCreator implements XMLFileCreator {
    List<Flower> flowers;

    public XMLDocumentFileCreator(List<Flower> flowers) {
        this.flowers = flowers;
    }

    /**
     * Creates dom tree from container of objects
     * Writes dom tree to file
     *
     * @param outputXmlFile file to write
     * @throws ParserConfigurationException Indicates configuration error
     * @throws TransformerException if cannot transform dom tree to stream
     */
    public void createXmlFile(String outputXmlFile) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = dbf.newDocumentBuilder();
            Document document = documentBuilder.newDocument();
            document.setXmlStandalone(true);
            Element root = document.createElement("flowers");
            document.appendChild(root);
            root.setAttribute("xmlns", "http://www.nure.ua");
            root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            root.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");

            for (Flower flower : flowers) {
                Element flowerTag = document.createElement("flower");
                addElementTo(flowerTag, document, flower.getName(), "name");
                addElementTo(flowerTag, document, flower.getSoil(), "soil");
                addElementTo(flowerTag, document, flower.getOrigin(), "origin");
                addVisualParametersElementTo(flowerTag, document, flower.getVisualParameters());
                addGrowingTipsElementTo(flowerTag, document, flower.getGrowingTips());
                addElementTo(flowerTag, document, flower.getMultiplying(), "multiplying");
                root.appendChild(flowerTag);
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(outputXmlFile));
            transformer.transform(domSource, streamResult);
        } catch (ParserConfigurationException | TransformerException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates growingTips tag that represent complex type element of Flower element
     *
     * @param flowerTag it provides Flower element from obtained container
     * @param document it is the root of the document tree
     * @param growingTips subelement of Flower element
     */
    private void addGrowingTipsElementTo(Element flowerTag, Document document, GrowingTips growingTips) {
        Element growingTipsElem = document.createElement("growingTips");
        Element tempreture = addElementTo(growingTipsElem, document, growingTips.getTempreture(), "tempreture");
        Element lighting = addElementTo(growingTipsElem, document, growingTips.getLighting(), "lighting");
        Element watering = addElementTo(growingTipsElem, document, growingTips.getWatering(), "watering");
        tempreture.setAttribute(growingTips.getTempretureAttr().getAttrName(), growingTips.getTempretureAttr().getAttrValue());
        lighting.setAttribute(growingTips.getLightingAttr().getAttrName(), growingTips.getLightingAttr().getAttrValue());
        watering.setAttribute(growingTips.getWateringAttr().getAttrName(), growingTips.getWateringAttr().getAttrValue());
        flowerTag.appendChild(growingTipsElem);
    }

    /**
     * Creates visualParameters tag that represent complex type element of Flower element
     *
     * @param flowerTag it provides Flower element from obtained container
     * @param document it is the root of the document tree
     * @param visualParameters subelement of Flower element
     */
    private void addVisualParametersElementTo(Element flowerTag, Document document, VisualParameters visualParameters) {
        Element visualParametersElem = document.createElement("visualParameters");
        addElementTo(visualParametersElem, document, visualParameters.getStemColour(), "stemColour");
        addElementTo(visualParametersElem, document, visualParameters.getLeafColour(), "leafColour");
        Element aveLenFlower = addElementTo(visualParametersElem, document, visualParameters.getAvelenFlower(), "aveLenFlower");
        String attrName = visualParameters.getAvelenFlowerAttr().getAttrName();
        String attrValue = visualParameters.getAvelenFlowerAttr().getAttrValue();
        aveLenFlower.setAttribute(attrName, attrValue);
        flowerTag.appendChild(visualParametersElem);
    }

    /**
     * Creates one simple type element of complex type element
     *
     * @param currentTag it provides element of complex type
     * @param document it is the root of the document tree
     * @param flowerProperty element's value of simple type
     * @param elementName element's name of simple type
     * @return element of complex type with created element of simple type
     */
    private Element addElementTo(Element currentTag, Document document, String flowerProperty, String elementName) {
        Element element = document.createElement(elementName);
        Text elementContent = document.createTextNode(flowerProperty);
        element.appendChild(elementContent);
        currentTag.appendChild(element);
        return element;
    }

    /**
     * Creates one simple type element of complex type element
     *
     * @param currentTag it provides element of complex type
     * @param document it is the root of the document tree
     * @param flowerProperty element's value of simple type
     * @param elementName element's name of simple type
     * @return element of complex type with created element of simple type
     */
    private Element addElementTo(Element currentTag, Document document, int flowerProperty, String elementName) {
        Element element = document.createElement(elementName);
        Text elementContent = document.createTextNode(String.valueOf(flowerProperty));
        element.appendChild(elementContent);
        currentTag.appendChild(element);
        return element;
    }
}
