package com.epam.rd.java.basic.task8.controller;

/**
 * Writes data to the output file and creates xml file
 *
 * @author Alexander Koreshev
 * @since 1.0
 */
public interface XMLFileCreator {
    void createXmlFile(String outputXmlFile);
}
