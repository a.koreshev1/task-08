package com.epam.rd.java.basic.task8.controller;

import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

/**
 * Validates obtained xml file by xsd file
 * Throws an exception if obtained files are incorrect
 *
 * @author Alexander Koreshev
 * @since 1.0
 */
public class XMLValidator {
    private final File xmlFile;
    private final File xsdFile;

    /**
     * Determines xsd and xml files
     * and check parameters for edge-cases
     *
     * @param xmlFileName string that represent name of xml file
     * @param xsdFileName string that represent name of xsd file
     * @throws NullPointerException     if anybody of files are null
     * @throws IllegalArgumentException if anybody of files are empty string
     */
    public XMLValidator(String xmlFileName, String xsdFileName) {
        if (xmlFileName == null || xsdFileName == null) {
            throw new NullPointerException(
                    "The input xml filename is " + xmlFileName + System.lineSeparator()
                            + "The input xsd filename is " + xsdFileName);
        }
        if (xmlFileName.isEmpty() || xsdFileName.isEmpty()) {
            throw new IllegalArgumentException(
                    "The input xml filename is empty" + System.lineSeparator()
                            + "The input xsd filename is empty");
        }
        this.xmlFile = new File(xmlFileName);
        this.xsdFile = new File(xsdFileName);
    }

    public XMLValidator(String xmlFileName, String xsdFileName, String pathToFiles) {
        this(pathToFiles + xmlFileName, pathToFiles + xsdFileName);
    }

    public XMLValidator(String pathToXml, String xmlFileName, String pathToXsd, String xsdFileName) {
        this(pathToXml + xmlFileName, pathToXsd + xsdFileName);
    }

    /**
     * Validates xml file by xsd file
     *
     * @return true when validation process cannot exceptions
     * @throws SAXException if the xsd file is wrong or the xml file is not valid
     * @throws IOException  if cannot read the xml file
     */
    public boolean validate() {
        try {
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Source schemaFile = new StreamSource(xsdFile);
            Schema schema = factory.newSchema(schemaFile);
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(xmlFile));
        } catch (SAXException | IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
