package com.epam.rd.java.basic.task8.flower;

/**
 * Represents element of complex type of xml tag 'flower'
 *
 * @author Alexander Koreshev
 * @since 1.0
 */
public class GrowingTips {
    int tempreture;
    String lighting;
    int watering;
    TagAttribute tempretureAttr;
    TagAttribute wateringAttr;

    public GrowingTips() {

    }

    public void setTempreture(int tempreture) {
        this.tempreture = tempreture;
    }

    public void setLighting(String lighting) {
        this.lighting = lighting;
    }

    public void setWatering(int watering) {
        this.watering = watering;
    }

    public void setTempretureAttr(TagAttribute tempretureAttr) {
        this.tempretureAttr = tempretureAttr;
    }

    public void setWateringAttr(TagAttribute wateringAttr) {
        this.wateringAttr = wateringAttr;
    }

    public void setLightingAttr(TagAttribute lightingAttr) {
        this.lightingAttr = lightingAttr;
    }

    public int getTempreture() {
        return tempreture;
    }

    public String getLighting() {
        return lighting;
    }

    public int getWatering() {
        return watering;
    }

    public TagAttribute getTempretureAttr() {
        return tempretureAttr;
    }

    public TagAttribute getWateringAttr() {
        return wateringAttr;
    }

    public TagAttribute getLightingAttr() {
        return lightingAttr;
    }

    TagAttribute lightingAttr;

    public GrowingTips(int tempreture, String lighting, int watering, TagAttribute tempretureAttr, TagAttribute wateringAttr, TagAttribute lightingAttr) {
        this.tempreture = tempreture;
        this.lighting = lighting;
        this.watering = watering;
        this.tempretureAttr = tempretureAttr;
        this.wateringAttr = wateringAttr;
        this.lightingAttr = lightingAttr;
    }

    @Override
    public String toString() {
        return "GrowingTips{" +
                "tempreture=" + tempreture +
                ", lighting='" + lighting + '\'' +
                ", watering=" + watering +
                ", tempretureAttr=" + tempretureAttr +
                ", wateringAttr=" + wateringAttr +
                ", lightingAttr=" + lightingAttr +
                '}';
    }
}
