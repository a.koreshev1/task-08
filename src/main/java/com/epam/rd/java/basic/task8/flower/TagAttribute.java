package com.epam.rd.java.basic.task8.flower;

/**
 * Storage name and value of attribute of xml tag
 *
 * @author Alexander Koreshev
 * @since 1.0
 */
public class TagAttribute {
    String attrName;
    String attrValue;

    public TagAttribute(String attrName, String attrValue) {
        this.attrName = attrName;
        this.attrValue = attrValue;
    }

    public TagAttribute() {

    }

    public void setAttrName(String attrName) {
        this.attrName = attrName;
    }

    public void setAttrValue(String attrValue) {
        this.attrValue = attrValue;
    }

    public String getAttrName() {
        return attrName;
    }

    public String getAttrValue() {
        return attrValue;
    }

    @Override
    public String toString() {
        return "Attribute{" +
                "attrName='" + attrName + '\'' +
                ", attrValue='" + attrValue + '\'' +
                '}';
    }
}