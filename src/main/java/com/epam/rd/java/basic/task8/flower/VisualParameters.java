package com.epam.rd.java.basic.task8.flower;

/**
 * Represents element of complex type of xml tag 'flower'
 *
 * @author Alexander Koreshev
 * @since 1.0
 */
public class VisualParameters {
    String stemColour;
    String leafColour;
    int avelenFlower;
    TagAttribute avelenFlowerAttr;

    public VisualParameters() {
    }

    public VisualParameters(String stemColour, String leafColour, int avelenFlower, TagAttribute avelenFlowerAttr) {
        this.stemColour = stemColour;
        this.leafColour = leafColour;
        this.avelenFlower = avelenFlower;
        this.avelenFlowerAttr = avelenFlowerAttr;
    }

    public String getStemColour() {
        return stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public int getAvelenFlower() {
        return avelenFlower;
    }

    public TagAttribute getAvelenFlowerAttr() {
        return avelenFlowerAttr;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public void setAvelenFlower(int avelenFlower) {
        this.avelenFlower = avelenFlower;
    }

    public void setAvelenFlowerAttr(TagAttribute avelenFlowerAttr) {
        this.avelenFlowerAttr = avelenFlowerAttr;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", avelenFlower=" + avelenFlower +
                ", avelenFlowerAttr=" + avelenFlowerAttr +
                '}';
    }
}
